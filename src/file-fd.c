#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>

/* Nombre de archivo */
#define archivo "archivo.txt"

int main(void)
{
  /* Descriptor de archivo */
  int fd;
  /* Apuntador hacia archivo */
  FILE* fp = NULL;

  /* Abriendo descriptor de archivo */
  fd = open(archivo, O_RDWR|O_APPEND|O_CREAT|O_TRUNC);
  /* Listando el número del descriptor de archivo */
  printf("El descriptor de archivos es: %d\n", fd);

  /* Abriendo apuntador a archivo */
  fp = fdopen(fd, "w+");
  /* Listando las propiedades del apuntador a archivo */
  printf("Las propiedades del apuntador a archivo son:\n");
  printf("\t_fileno:\t%d\n", fp->_fileno);
  printf("\t_flags:\t%#x\n", fp->_flags);
  printf("\t_flags2:\t%#x\n", fp->_flags2);

  /* Cerrando apuntador a archivo */
  fclose(fp);
  /* Cerrando descriptor de archivo */
  close(fd);

  return EXIT_SUCCESS;
}
